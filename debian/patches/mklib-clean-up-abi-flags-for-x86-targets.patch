From: Mike Frysinger <vapier@gentoo.org>
Date: Tue, 11 Sep 2012 01:57:25 -0400
Subject: mklib: clean up abi flags for x86 targets

The current code is duplicated in two places and relies on `uname` to
detect the flags.  This is no good for cross-compiling, and the current
logic uses -m64 for the x32 ABI which breaks things.

Unify the code in one place, avoid `uname` completely, and add support
for the new x32 ABI.

Signed-off-by: Mike Frysinger <vapier@gentoo.org>
---
 mklib | 49 ++++++++++++++++++++++++++-----------------------
 1 file changed, 26 insertions(+), 23 deletions(-)

diff --git a/mklib b/mklib
index 8e47a27..c6a2423 100755
--- a/mklib
+++ b/mklib
@@ -217,6 +217,25 @@ case $ARCH in
             fi
 	fi
 
+	# Check if objects are 32-bit and we're running in 64-bit
+	# environment.  If so, pass -m32 flag to linker.
+	add_abi_flag_to_opts() {
+	    case $(file $1) in
+		*32-bit*x86-64*)
+		    # x86_64 x32 ABI.
+		    OPTS="-mx32 ${OPTS}"
+		    ;;
+		*64-bit*x86-64*)
+		    # x86_64 64-bit ABI.
+		    OPTS="-m64 ${OPTS}"
+		    ;;
+		*32-bit*Intel*)
+		    # x86 32-bit ABI.
+		    OPTS="-m32 ${OPTS}"
+		    ;;
+	    esac
+	}
+
 	if [ $NOPREFIX = 1 ] ; then
 	    # No "lib" or ".so" part
 	    echo "mklib: Making" $ARCH "shared library: " ${LIBNAME}
@@ -228,13 +247,8 @@ case $ARCH in
 	    ;;
 	    esac
 
-	    # Check if objects are 32-bit and we're running in 64-bit
-	    # environment.  If so, pass -m32 flag to linker.
-	    set ${OBJECTS}
-	    ABI32=`file $1 | grep 32-bit`
-	    if [ "${ABI32}" -a `uname -m` = "x86_64" ] ; then
-		OPTS="-m32 ${OPTS}"
-	    fi
+            # Check to see if we are building for a different ABI.
+            add_abi_flag_to_opts ${OBJECTS}
 
             if [ "${ALTOPTS}" ] ; then
                 OPTS=${ALTOPTS}
@@ -281,13 +295,9 @@ case $ARCH in
 		# exptmp is removed below
 	    fi
 
-	    # Check if objects are 32-bit and we're running in 64-bit
-	    # environment.  If so, pass -m32 flag to linker.
-	    set ${OBJECTS}
-	    ABI32=`file $1 | grep 32-bit`
-	    if [ "${ABI32}" -a `uname -m` = "x86_64" ] ; then
-		OPTS="-m32 ${OPTS}"
-	    fi
+            # Check to see if we are building for a different ABI.
+            add_abi_flag_to_opts ${OBJECTS}
+
             if [ "${ALTOPTS}" ] ; then
                 OPTS=${ALTOPTS}
             fi
@@ -355,15 +365,8 @@ case $ARCH in
 		OPTS="-G"
 	    else
 		# gcc linker
-		# Check if objects are 32-bit and we're running in 64-bit
-		# environment.  If so, pass -m32 flag to linker.
-		set ${OBJECTS}
-		ABI32=`file $1 | grep 32-bit`
-		if [ "${ABI32}" ] ; then
-		    OPTS="-m32 -shared -Wl,-Bdynamic"
-		else
-		    OPTS="-m64 -shared -Wl,-Bdynamic"
-		fi
+		# Check to see if we are building for a different ABI.
+		add_abi_flag_to_opts ${OBJECTS}
 	    fi
 
 	    # Check if objects are SPARC v9
